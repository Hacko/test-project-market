// function getFullName(firstName, middleName, lastName) {
//     return [firstName, lastName, middleName].join(' ');
// }
//
// function getAddress(country, city, street, house) {
//     // Россия, Ростов-на-Дону, ул. Большая Садовая, 119
//     return [country, city, street, house].join(', ');
// }
//
// function getContacts(phoneNumber, email) {
//     return 'Тел: ' + phoneNumber + '; Email: ' + email;
// }
//
var t = {
    firstName: "",
    lastName: "",
    gender: "",
    getFullName: function () {
        return [this.firstName, this.lastName].join(' ');
    }
};
// // var user = new Object();
//
// t.firstName = "Eugene";
// t.lastName = "Hatsko";
// t.gender = true;

function User(firstName, lastName, gender) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.comments = [];

    this.getFullName = function () {
        return [this.firstName, this.lastName].join(' ');
    };

    this.addComment = function(comment) {
        this.comments.push(comment);
    };

    return this;
}

var users = [];
users.push(new User("Dmitry", "Petrov", true));
users.push(new User("Alexander", "Ivanov", true));

// user2.getShortName = function () {
//     return [this.firstName, this.lastName[0] + '.'].join(' ');
// };

// function Category(id, name) {
//     this.id = id;
//     this.name = name;
// }




// console.log(user1.getFullName());
// console.log(user1.firstName);
//
// user1.firstName = "Dmitry1";
// console.log(user1.getFullName());
// console.log(user1.firstName);
//
// user1.firstName = "Dmitry2";
// console.log(user1.getFullName());
// console.log(user1.firstName);

